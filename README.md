# OUM Satis

This is the Open Up Media private composer repository, build with [Satis](https://github.com/composer/satis).

Our setup is based on this [repo]( https://gitlab.com/sandfox/satis/), used in this [article](https://sandfox.me/php/satis-gitlab.html)
